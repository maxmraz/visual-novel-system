# Image

Contains information about what image to display, how to display it, and where to display it. Note that if you do not specify an image we will create an invisible surface instead.

## Applicable Objects

- [name_box](name_box.md)
- [dialog box](dialog_box.md)
- [characters](characters.md)
- [background](background.md)
- [question](question.md)

## Usage

They are specified under `image` element under each of the objects config.

## Attributes

|name|default|values|description|
|----|-------|------|-----------|
|path|     |      | path (relative to sprites directory)|
|position|default |see description| check position section|
|x_offset|0 |Any Integer|applies X offset to image position|
|y_offset|0 |Any Integer|applies Y offset to image position|
|relative_to_dialog_box|false| boolean| calc position relative to dialog box|
|sprite| |see description| check sprites section|

### Position

Manually specifying every displayed object's xy is not only time consuming, but also very brittle. Instead we use positions and let the VN Manager do the hard job of calculating XY positons. By default position is calculated using the displayed window size. However in certain cases another objects can be used instead (e.g. A dialog box)

The diagram below shows you all of the positions that are available.

```text
            outsidetopleft            outsidetop         outsidetopright
            +-----------------------------------------------------------+
            |topleft, reset               top                   topright|
            |                                                           |
            |                                                           |
            |                                                           |
            |                                                           |
outsideleft |                          truecenter                       | outsideright
            |                                                           |
            |                                                           |
            |                                                           |
            |                                                           |
            |left                   center, default                right|
            +-----------------------------------------------------------+
            outsidebottomleft        outsidebottom    outsidebottomright
```

### Sprites

We use the sprite table to denote that the image we're specifying is a sprite. If the sprite table doesn't exist. The Visual Novel System assumes you want to load an image.

Inside the sprite table you can include two optional parameters `animation` and `direction` Which allow you to specify the animation and direction of the sprite you wish to use. However if you are using the default animation and direction they are not required.
See [Solarus Sprites](https://www.solarus-games.org/doc/latest/lua_api_sprite.html)

## Examples

### No name_box

```lua
dialog_box = {
  name_box = {
    image = {
      path = "",
      position = "center"
    }
  }
}
```

![Example Invisible name_box With Custom Placement](screenshots/namebox/invisble_name_box.png)

### dialog box with static image at true center

```lua
dialog_box = {
  image = {
    position = "truecenter",
    path = "demo/hud/dialog_boxes/dialog_box.png"
  }
}
```

![Example Static Dialog Box](screenshots/images/static_dialog_box.png)

### animated name_box at center as a sprite

```lua
dialog_box = {
  name_box = {
    image = {
      path = "demo/hud/dialog_boxes/animated_name_box",
      position = "center",
      sprite = {}
    },
    line = {
      y_offset = -8
    }
  }
}
```

![Example Animated name_box](screenshots/images/animated_name_box.gif)
