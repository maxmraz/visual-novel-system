# Name_Box

Options around displaying the name_box. A name_box is laid the same
as a dialog box with the following caveats.

- its' position is always relative to the dialog box
- midline options are NOT available
- single line only
- will not appear until characters are specified in dialog

## Applicable Objects

- [dialog_box](dialog_box.md)

## Contains Objects

- [transition](transition.md)
- [image](image.md)
- [line](line.md)

## Examples

### No name_box

```lua
dialog_box = {
  name_box = {
    image = {
      path = "",
      position = "center"
    }
  }
}
```

![Example Invisible name_box With Custom Placement](screenshots/namebox/invisble_name_box.png)

### Static Namebox at outside top right

```lua
dialog_box = {
  name_box = {
    image = {
      position = "outsidetopright",
      path = "demo/hud/dialog_boxes/name_box.png",
    }
  }
}
```

![Example name_box With Custom Placement](screenshots/namebox/outside_top_left.png)

### Animated Namebox at Center

```lua
dialog_box ={
  name_box = {
    image = {
      path = "demo/hud/dialog_boxes/animated_name_box",
      position = "center",
      sprite = {}
    },
    line ={
      y_offset = -8
    }
  }
}
```

![Example Animated name_box](screenshots/images/animated_name_box.gif)
