# Multple Languages Guide

Solarus Quests were developed to quickly and easily be translated to multiple langauges. They've even provided a [guide](https://www.solarus-games.org/doc/latest/translation.html) over it.

The Visual Novel System further augments this by giving you the ability to customize your dialogs on a per language basis. By adding to configs to the Solarus `languages/YOUR_LANGUAGE/configs` folder.

In addition you can customize your character names using the inbuilt Solarus methodology. Please see the [characters](characters.md) section over localizing character names for more infomration.

Your configs are laid out exactly the same as in the [scene](scene.md). Configs in the language folder override the scene folder even if the config would normally take precedence.

Here's an example of what I mean:

Say we have a config located at: `/data/scripts/dialogs/configs/scenes/my/cool/scene.config`

And another located at: `/data/languages/en/configs/scenes/default.lua`

Any keys specified in `/data/languages/en/configs/scenes/default.lua` would win out.

## Example

Say we have added a new language. Japanese in this case:

!['Japanese Language Screenshot](screenshots/multiple_langauges_guide/new_language_added.png)

Then we need to add a config folder underneath the language like so (NOTE you may not be able to create this from the Solarus Quest Editor so just manually create the folder if you have to)

!['Config Folder Added Screenshot](screenshots/multiple_langauges_guide/config_added.png)

A common thing that needs changed between languages is font. So let's add  to the `default.lua`

!['Default Lua File Added Screenshot](screenshots/multiple_langauges_guide/default_lua_added.png)

Now if we change our language to `jp` and have added the appropriate translation. Then it will update to the font we specified.

!['Dialog Text Changed Screenshot](screenshots/multiple_langauges_guide/dialog_text_change.png)
