# Color

A quick way to set colors for dialog lines. This can be done via a color name or an RGB value.

## Applicable Objects

- [line](line.md)
- [midline_options](midline_options.md)

## By Name

color names available are:

- white
- red
- green
- blue
- yellow
- pink
- black

## By RGB Value

You can also enter a three element array corresponding to it's [RGB](https://www.rapidtables.com/web/color/RGB_color.html) values.
e.g. [255,0,255] (for purple)

## Examples

### Change name_box Text To Red Via Name

```lua
dialog_box = {
  name_box = {
    line = {
      color = 'red'
    }
  }
}
```

### Change Dialog Box Text To Purple Via RGB Values

```lua
dialog_box = {
  text = {
    line = {
      color = {255,0,255}
    }
  }
}
```
