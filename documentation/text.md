# Text

Options around positioning text within a dialog box.

## Applicable Objects

- [dialog_box](dialog_box.md)

## Contains Objects

- [question](question.md)
- [line](line.md)

## Attributes

|name|default|values|description|
|----|-------|------|-----------|
|max_displayed_lines|4|Any Positive Integer|The max number of lines to display at a time|
|x_offset|8|Any Positive Integer|The x offset so the text appears inside the dialog box border
|y_offset|8|Any Positive Integer|The y offset so the text appears inside the dialog box border
|line_space|14|Any Positive Integer|The space between each line|

## Examples

### allowing for 5 lines and some custom offsets

```lua
dialog_box = {
  text = {
    max_displayed_lines = 5,
    x_offset = 7,
    y_offset = 12
  }
}
```

![Example Text Lines and Offset](screenshots/text/dialog_offset_and_extra_lines.gif)
