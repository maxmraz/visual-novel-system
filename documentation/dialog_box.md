# Dialog_Box

Handles all customizations relating to the dialog box. The dialog box
has by far the most children owing to the sheer amount of ways to customize
it. I've split them up into various child markdown files for readability.
All elements can be used in tandem to allow you compete control over how to
customize it.

You can also hide the dialog_box during gameplay via the `H` key.
This is hardcoded at the moment and cannot be changed.

## Contains Objects

- [name_box](name_box.md)
- [image](image.md)
- [text](text.md)

## Usage

Attributes are specified under `dialog_box` element under each of the objects config.

## Attributes

|name|default|values|description|
|----|-------|------|-----------|
|close_delay|     | Any Positive Integer | see close_delay section|

### close_delay

It's sometimes handy to delay closing the dialog box.(e.g. you'd like
the question box to have a chance to display it's answer cursor).

You do NOT specify transition delays here. [See transitions](transitions.md)
If both a transition and close_delay are specified. The transition delay will happen
first then the close delay. (E.g. if your transition delay is 10ms and your close
delay is 30ms your box will take 40ms to close)

## Examples

### No Dialog Box

```lua
  dialog_box = {
    image = {
      path = ""
    }
  }
```

![No Dialog Box Image](screenshots/dialog_box/no_box_image.png)

### With Dialog Box Static Image

```lua
dialog_box = {
    image = {
      path = "demo/hud/dialog_boxes/dialog_box.png",
    },
}
```

![Box Image](screenshots/dialog_box/box_image_with_default_position.png)

### With Custom Position

```lua
dialog_box = {
    image = {
      position = "topleft",
      path = "demo/hud/dialog_boxes/dialog_box.png",
    },
}
```

![Box Image In Upper Left Position](screenshots/dialog_box/box_in_upper_left.png)

### With Position and X and Y Offsets

```lua
dialog_box = {
  image = {
    position = "truecenter",
    x_offset = 10,
    y_offset = -15
  }
}
```

![Box Image With Custom Position](screenshots/dialog_box/box_with_custom_position.png)

### With Animated Dialog Box

```lua
dialog_box = {
  image = {
    path = "demo/hud/dialog_boxes/animated_dialog_box",
    sprite = {
      animation = "blur",
      direction = 0
    }
  }
}
```

![Box Animation](screenshots/dialog_box/animated_dialog_box.gif)
