# Scene

The scene is the top level config containing all the of the elements needed for the scene to play.

## Contains Objects

- [characters](characters.md)
- [dialog_box](dialog_box.md)
- [background](background.md)

## Usage

At least one scene config is needed for VNS to work. This is the `default.lua` and it's located at `data/scripts/dialog/configs/scenes/default.lua`.

![default file location](screenshots/scene/default_file_location.png)

This default will be loaded for every scene, however options can be overridden piecemeal or in whole by other parts of the VNS system.

## Stacking configs

Config Stacking is where VNS really shines. Once you have your defaults set you can override those values based on character, scene, or both.

## Scene Override Example

Assume I have a dialog called `max.greeting`
(information about Solarus dialogs can be found [here](https://www.solarus-games.org/doc/0.9.1/dialog_syntax.html))
 and I have a default.lua that looks like this:

```lua
background = {
  image = {
    path = "default"
  }
}

dialog_box = {
  close_delay = 0,
    image = {
      position = "center",
      path = "demo/hud/dialog_boxes/dialog_box.png"
    }
  }
}
```

Under our `configs/scene` folder we can create a new folder called `max`
in it we can create a new lua file called `config.lua`

Our config.lua is this:

```lua
background = {
  image = {
    path = "path/to/my/cool/background"
  }
}
```

When the `max.greeting` is called the resulting config would be:

```lua
background = {
  image = {
    path = "path/to/my/cool/background"
  }
}

dialog_box = {
  close_delay = 0,
    image = {
      position = "center",
      path = "demo/hud/dialog_boxes/dialog_box.png"
    }
  }
}
```

Notice that background image path changes to the `config.lua` value while keeping all the other values. If you were to create a `greetings` folder under `max` and add a `config.lua` in it. Any values it had would take precedence over the ones in max.

## Character Override Example

please see [characters](characters.md) for more details.

## Config Heirarchy

The order of prcedence from lowest to highest is:

- characters/default.lua
- characters/CHARACTER_NAME.lua
- scene/default.lua
- scene/PATH/TO/SCENE/config.lua

## Examples

This is an example default.lua

```lua
background = {
  image = {
    path = "default"
  }
}
dialog_box = {
  close_delay = 0,
   image = {
    position = "center",
    path = "demo/hud/dialog_boxes/dialog_box.png"
  },
  text = {
    max_displayed_lines = 4,
    x_offset = 24,
    y_offset = 20,
    line_space = 14,
    line = {
      speed = "fast",
      horizontal_alignment = "left",
      vertical_alignment =  "middle",
      font = "ComicNeue-Angular-Bold",
      font_size = 12
    },
    question = {
      line_buffer = 7,
      question_marker = "$?",
      cursor_wrap = true,
      cursor = {
        image = {
          path = "demo/hud/cursor/icons/individual_icons/book_04e.png",
          x_offset = 0,
          y_offset = -8
        }
      }
    }
  },
  name_box = {
    image = {
      path = "demo/hud/dialog_boxes/name_box.png",
      position = "outsidetopleft"
    },
    line = {
      x_offset = 3,
      y_offset = 10,
      horizontal_alignment = "left",
      vertical_alignment = "middle",
      font = "ComicNeue-Angular-Bold",
      font_size = 10
    }
  }
}
```
